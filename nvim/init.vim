lua require('init')

" search
set hlsearch incsearch ignorecase smartcase
set timeoutlen=1000 ttimeoutlen=10
set noshowmode
set winaltkeys=no

set shortmess+=c

filetype plugin indent on

" todo command
command! Todo noautocmd vimgrep /TODO\|FIXME/j ** | cw

" vim setting
let g:tex_flavor = 'latex'

let g:python3_host_prog = '/bin/python3'

