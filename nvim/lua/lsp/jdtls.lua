local finders = require('telescope.finders')
local sorters = require('telescope.sorters')
local actions = require('telescope.actions')
local pickers = require('telescope.pickers')
local action_state = require('telescope.actions.state')

local M = {}

local project_name = vim.fn.fnamemodify(vim.fn.getcwd(), ':p:h:t')

local workspace_dir = '/home/dante/workspace/java/' .. project_name


local on_attach = function(_, bufnr)
	local function buf_set_keymap(...)
		vim.api.nvim_buf_set_keymap(bufnr, ...)
	end
	local opts = { noremap = true, silent = true }

	buf_set_keymap("n", "gd", "<cmd>lua require'telescope.builtin'.lsp_definitions()<CR>", opts)
	buf_set_keymap("n", "gTD", "<cmd>lua vim.lsp.buf.type_definition()<CR>", opts)
	buf_set_keymap("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
	buf_set_keymap('n', 'gc', "<cmd>lua vim.lsp.buf.code_action()<CR>", opts)
	buf_set_keymap("n", "ge", "<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>", opts)
	buf_set_keymap("n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>", opts)
	buf_set_keymap("i", "gS", "<cmd>lua vim.lsp.buf.signature_help()<CR>", opts)
	buf_set_keymap("n", "[g", "<cmd>lua vim.lsp.diagnostic.goto_next()<CR>", opts)
	buf_set_keymap("n", "]g", "<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>", opts)
	buf_set_keymap("n", "gr", "<cmd>lua require'telescope.builtin'.lsp_references()<CR>", opts)
	buf_set_keymap("n", "<leader>ac", "<cmd>lua RTELE(); require'telescope.builtin'.lsp_document_symbols{}<CR>", opts)
	buf_set_keymap('n', '<Leader>r', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
	buf_set_keymap('n', '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
	buf_set_keymap('n', '<Leader>di', "<Cmd>lua require'jdtls'.organize_imports()<CR>", opts)
	buf_set_keymap('n', '<Leader>dt', "<Cmd>lua require'jdtls'.test_class()<CR>",opts )
	buf_set_keymap('n', '<Leader>dn', "<Cmd>lua require'jdtls'.test_nearest_method()<CR>",opts)
	buf_set_keymap('v', '<Leader>de', "<Esc><Cmd>lua require('jdtls').extract_variable(true)<CR>", opts)
	buf_set_keymap('n', '<Leader>de', "<Cmd>lua require('jdtls').extract_variable()<CR>", opts)
	buf_set_keymap('v', '<Leader>dm', "<Esc><Cmd>lua require('jdtls').extract_method(true)<CR>", opts)
	buf_set_keymap("n", "<Leader>ge", "<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>",opts )

	require('jdtls').setup_dap({ hotcodereplace = 'auto' })
end

function M.setup()
    local config = {
        flags = {
            -- allow_incremental_sync = true,
            server_side_fuzzy_completion = true,
        },
        -- capabilities = java.lsp.capabilities,
        -- on_attach = java.lsp.on_attach,
    }

    local extendedClientCapabilities = require('jdtls').extendedClientCapabilities
    extendedClientCapabilities.resolveAdditionalTextEditsSupport = true

    config.init_options = {
        extendedClientCapabilities = extendedClientCapabilities,
        bundles = {
            vim.fn.glob(
                '/home/dante/.local/share/dap/java-debug/com.microsoft.java.debug.plugin/target/com.microsoft.java.debug.plugin-*.jar'
            ),
        },
    }

    config.settings = {
        -- ['java.format.settings.url'] = home .. '/.config/nvim/language-servers/java-google-formatter.xml',
        -- ['java.format.settings.profile'] = 'GoogleStyle',
        java = {
            signatureHelp = { enabled = true },
            contentProvider = { preferred = 'fernflower' },
            completion = {
                favoriteStaticMembers = {
                    -- 'org.hamcrest.MatcherAssert.assertThat',
                    -- 'org.hamcrest.Matchers.*',
                    -- 'org.hamcrest.CoreMatchers.*',
                    -- 'org.junit.jupiter.api.Assertions.*',
                    -- 'java.util.Objects.requireNonNull',
                    -- 'java.util.Objects.requireNonNullElse',
                    -- 'org.mockito.Mockito.*',
                },
            },
            sources = {
                organizeImports = {
                    starThreshold = 9999,
                    staticStarThreshold = 9999,
                },
            },
            codeGeneration = {
                generatpComments = true,
                -- toStrinvag = {
                --     template = '${object.className}{${member.name()}=${member.value}, ${otherMembers}}',
                -- },
            },
            configuration = {
                runtimes = {
                    {
                        name = 'JavaSE-11',
                        path = '/usr/lib/jvm/java-11-openjdk-amd64/',
                    },
                },
            },
        },
    }

    -- config.cmd = { 'java-lsp.sh', workspace_folder }

	config.cmd = {
		-- 💀
		'java', -- or '/path/to/java11_or_newer/bin/java'
				-- depends on if `java` is in your $PATH env variable and if it points to the right version.

		'-Declipse.application=org.eclipse.jdt.ls.core.id1',
		'-Dosgi.bundles.defaultStartLevel=4',
		'-Declipse.product=org.eclipse.jdt.ls.core.product',
		'-Dlog.protocol=true',
		'-Dlog.level=ALL',
		'-Xms1g',
		'--add-modules=ALL-SYSTEM',
		'--add-opens', 'java.base/java.util=ALL-UNNAMED',
		'--add-opens', 'java.base/java.lang=ALL-UNNAMED',
		-- 💀
		'-jar', '/home/dante/workspace/external_repos/eclipse.jdt.ls/org.eclipse.jdt.ls.product/target/repository/plugins/org.eclipse.equinox.launcher_1.6.400.v20210924-0641.jar',
			 -- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                                       ^^^^^^^^^^^^^^
			 -- Must point to the                                                     Change this to
			 -- eclipse.jdt.ls installation                                           the actual version
		-- 💀
		'-configuration', '/home/dante/workspace/external_repos/eclipse.jdt.ls/org.eclipse.jdt.ls.product/target/repository/config_linux',
						-- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^        ^^^^^^
						-- Must point to the                      Change to one of `linux`, `win` or `mac`
						-- eclipse.jdt.ls installation            Depending on your system.
		-- 💀
		-- See `data directory configuration` section in the README
		'-data', '.cache/'
	}
	config.root_dir= require('jdtls.setup').find_root({'.git', 'mvnw', 'gradlew'})
    config.on_attach = on_attach
    config.on_init = function(client, _)
	client.notify('workspace/didChangeConfiguration', { settings = config.settings })
    end

    require('jdtls.ui').pick_one_async = function(items, prompt, label_fn, cb)
        local opts = {}
        pickers.new(opts, {
            prompt_title = prompt,
            finder = finders.new_table({
                results = items,
                entry_maker = function(entry)
                    return {
                        value = entry,
                        display = label_fn(entry),
                        ordinal = label_fn(entry),
                    }
                end,
            }),
            sorter = sorters.get_generic_fuzzy_sorter(),
            attach_mappings = function(prompt_bufnr)
                actions.select_default:replace(function()
                    local selection = action_state.get_selected_entry()
                    actions.close(prompt_bufnr)
                    cb(selection.value)
                end)

                return true
            end,
        }):find()
    end
    -- Server
    require('jdtls').start_or_attach(config)
end


return M
