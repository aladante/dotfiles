local install_path = vim.fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  vim.cmd("!git clone https://github.com/wbthomason/packer.nvim " .. install_path)
end

return require "packer".startup(
  function(use)
    -- Packer can manage itself as an optional plugin
	use {"nvim-lua/popup.nvim"}
	use {"wbthomason/packer.nvim"}

	use {"christoomey/vim-tmux-navigator"}
	use {"hoob3rt/lualine.nvim"}
	use {"akinsho/nvim-bufferline.lua"}

	use {"nvim-telescope/telescope.nvim"}
	use {"nvim-lua/plenary.nvim"}
	use {"kyazdani42/nvim-web-devicons"}

	-- Git
	use {"tpope/vim-fugitive"}
	use {"tpope/vim-rhubarb"}

	-- LSP
	use {"neovim/nvim-lspconfig"}
	use {
		'nvim-treesitter/nvim-treesitter',
		run = ':TSUpdate'
	}
	use {"nvim-lua/lsp-status.nvim"}
	use {"ray-x/lsp_signature.nvim"}

	-- cmp
	use {'hrsh7th/cmp-nvim-lsp'}
	use{'hrsh7th/cmp-buffer'}
	use{'hrsh7th/cmp-path'}
	use{'hrsh7th/cmp-cmdline'}
	use{'hrsh7th/nvim-cmp'}

	-- snippets
	use {'hrsh7th/cmp-vsnip'}
	use{'hrsh7th/vim-vsnip'}
	use{'L3MON4D3/LuaSnip'}
	use{'SirVer/ultisnips'}
	use{'honza/vim-snippets'}
		--
	-- Java
	use {"mfussenegger/nvim-jdtls"}
	use{"mfussenegger/nvim-dap"}
	use{"rcarriga/nvim-dap-ui"}

	-- Theme
	use{"rebelot/kanagawa.nvim"}
  end
)

