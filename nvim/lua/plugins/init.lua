-- ui
require "plugins.bufferline"
require "plugins.lualine"
require "plugins.telescope"

-- lsp support
require "plugins.tree-sitter"
require "plugins.nvim-cmp"
require "plugins.nvim-dap"


-- COLOR SCHEME
require "plugins.colorscheme"
-- java
--require "plugins.java"
