#!/usr/bin/env bash
TMP_BG=/tmp/screenshot.png
#
# Take screenshot
scrot $TMP_BG
# Pixelate screenshot
convert $TMP_BG -scale 10% -scale 1000% -fill black -colorize 25% $TMP_BG
i3lock -i $TMP_BG -f -n

