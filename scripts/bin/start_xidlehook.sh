#/bin/bash

pkill xidlehook

# Run xidlehook
xidlehook \
  --not-when-fullscreen \
  --not-when-audio \
  --timer 600 \
    'i3lock -c 000000' \
    ''\  
	--timer 3600 \
    'systemctl suspend' \
