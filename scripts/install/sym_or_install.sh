#!/usr/bin/env bash

# [root file in DOTFILES_ROOT] [LOCATION SYMLINK]

declare -A SYMLINKS=(

# .CONFIG FILES
    ['autostart/sxhkd.desktop']='.config/autostart/sxhkd.desktop'
    ['dunst/dunstrc']='.config/dunst/dunstrc'
    ['htop/htoprc']='.config/htop/htoprc'
    ['neofetch/config.conf']='.config/neofetch/config.conf'
    ['rofi/config.rasi']='.config/rofi/config.rasi'
    ['rofi/themes/slate.rasi']='.config/rofi/themes/slate.rasi'
    ['mimeapps/mimeapps.list']='.config/mimeapps.list'
    ['sxhkd/sxhkdrc']='.config/sxhkd/sxhkdrc'
    ['coc/package.json']='.config/coc/extensions/package.json'
    ['bspwm/bspwmrc']='.config/bspwm/bspwmrc'
    ['picom/picom.conf']='.config/picom/picom.conf'
    ['polybar/config']='.config/polybar/config'

# .CONFIG FOLDERS
    ['nvim']='.config/nvim' 
    ['kitty']='.config/kitty'

# ROOT FILES
    ['zsh/zshrc']='.zshrc'
    ['X/xinitrc']='.xinitrc'
    ['X/Xresources']='.Xresources'
    ['ripgrep/ripgreprc']='.ripgreprc'
    ['tmux/tmux.conf']='.tmux.conf'
    ['git/gitconfig']='.gitconfig'
    ['git/gitignore']='.gitignore'

# FONT
    ['font/jetbrains']='.local/share/fonts/jetbrains'

# SCRIPTS
    ['scripts/bin/switch-or-run.sh']='bin/switch-or-run'
    ['scripts/bin/start_xidlehook.sh']='bin/start_xidlehook'
    ['scripts/bin/update.sh']='bin/update'
    ['scripts/bin/lock.sh']='bin/lock'
    ['scripts/bin/polybar_launch.sh']='bin/polybar_launch'
    ['scripts/bin/bspscratchpad.sh']='bin/bspscratchpad'
)


DOTFILES_ROOT=~/dotfiles

user()    { printf "\r  [ \033[0;33m?\033[0m ] $1 " ; }
success() { printf "\r\033[2K  [ \033[00;32mOK\033[0m ] $1\n" ; }
fail()    { printf "\r\033[2K  [\033[0;31mFAIL\033[0m] $1\n" ; echo '' ; exit ; }
link_file() { mkdir -p "`dirname "$2"`" ; ln -s "$1" "$2" ; success "linked $1 to $2" ; }
copy_file() { mkdir -p "`dirname "$2"`" ; cp -r "$1" "$2" ; success "copied $1 to $2" ; }

echo 'Installing public dotfiles...'

fresh_install() {
	# To fully function you need to install xidlehook with cargo 

	sudo apt-get update -y
	sudo apt-get upgrade -y 
	sudo apt install curl wget -y
	sudo apt-get dist-upgrade -y

	sudo apt-get install -y\
		zathura \
		kitty \
		vlc \
		feh \
		curl \
		imagemagick \
		scrot \
		neofetch \
		vim \
		zsh \
		tmux \
		xinit \
		ripgrep \
		rofi \
		htop \
		sxhkd \
		libpulse-dev \
		libxss-dev \
		libxcb-screensaver0-dev \
		pulseaudio \
		cargo \
		thermald \
		firmware-linux-nonfree \
		bspwm \
		picom \
		polybar \
		lm-sensors \
		xbacklight \ 
		tor 
}

do_symlinks() {
    echo 'Symlinks...'

    # remove dead symlinks
    for i in $(file .* | grep broken | cut -d : -f 1); do rm $i; done

    overwrite_all=false
    backup_all=false
    skip_all=false
    for K in "${!SYMLINKS[@]}"; do
        source="${DOTFILES_ROOT}/${K}"
        dest="${HOME}/${SYMLINKS[$K]}"

        if [ ! -f "$source" ] && [ ! -d "$source" ]; then
            fail "File $source does not exists"
        fi

        if [ -f "$dest" ] || [ -d "$dest" ]; then
          overwrite=false
          backup=false
          skip=false
          if [ "$overwrite_all" == "false" ] && [ "$backup_all" == "false" ] && [ "$skip_all" == "false" ]; then
            user "File already exists: $dest, what do you want to do? [s]kip, [S]kip all, [o]verwrite, [O]verwrite all, [b]ackup, [B]ackup all?"
            read -n 1 action

            case "$action" in
              o )
                overwrite=true;;
              O )
                overwrite_all=true;;
              b )
                backup=true;;
              B )
                backup_all=true;;
              s )
                skip=true;;
              S )
                skip_all=true;;
              * )
                ;;
            esac
          fi

          if [ "$overwrite" == "true" ] || [ "$overwrite_all" == "true" ]; then
            rm -rf "$dest"
            success "removed $dest"
          fi

          if [ "$backup" == "true" ] || [ "$backup_all" == "true" ]; then
            mv "$dest" "$dest\.backup"
            success "moved $dest to $dest.backup"
          fi

          if [ "$skip" == "false" ] && [ "$skip_all" == "false" ]; then
            link_file "$source" "$dest"
          else
            success "skipped $source"
          fi

        else
          if [ -L "$dest" ]; then
            rm "$dest"
          fi
          link_file "$source" "$dest"
        fi
    done
}

user "Do you want to [i]nstall or [s]ymlink"
read -n 1 options 
case "$options" in
  i )
	do_symlinks	&& fresh_install;;
  s )
	do_symlinks;;
esac
